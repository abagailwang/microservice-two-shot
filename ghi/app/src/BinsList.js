import React from 'react';
import { Link } from "react-router-dom";

// not currently in use by app

function BinsList({ bins, getBins }) {
    const deleteBin = async (id) => {
        const response = await fetch(`http://localhost:8100/api/bins/${id}/`, {
            method: "delete",
        })
        if (response.ok) {
            getBins()
        }
    }

    if (bins === undefined) {
        return null
    }




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Bin's closet name</th>
              <th>Bin Number</th>
              <th>Bin size</th>
              <th>Delete pair</th>


            </tr>
          </thead>
          <tbody>
            {bins.map(bin => {
                return (
                    <tr key={bin.id}>
                        <td>{ bin.closet_name }</td>
                        <td>{ bin.bin_number }</td>
                        <td>{ bin.bin_size }</td>
                        <td>
                            <button type="button" value={bin.id} onClick={() => deleteBin(bin.id)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default BinsList;
