import React, {useEffect, useState} from 'react';

function LocationsForm(props) {
    const [closetName, setClosetName] = useState('');
    const [sectionNumber, setSectionNumber] = useState('');
    const [shelfNumber, setShelfNumber] = useState('');


    const handleClosetNameChange = (event) => {
        const value = event.target.value;
        setClosetName(value);
    }

    const handleSectionNumberChange = (event) => {
        const value = event.target.value;
        setSectionNumber(value);
    }

    const handleShelfNumberChange = (event) => {
        const value = event.target.value;
        setShelfNumber(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.closet_name = closetName;
        data.section_number = sectionNumber;
        data.shelf_number = shelfNumber;

        console.log(data);

        const locationUrl = 'http://localhost:8100/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            setClosetName('');
            setSectionNumber('');
            setShelfNumber('');
        }

    }






// foreign key relationship code from ShoesForm
    // const fetchData = async () => {
    //     const url = 'http://localhost:8100/api/bins/';
    //     const response = await fetch(url);
    //     if (response.ok) {
    //       const data = await response.json();
    //       setBins(data.bins);
    //     }
    //   }

    //   useEffect(() => {
    //     fetchData();
    //   }, []);


return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://homesthetics.net/wp-content/uploads/2016/12/Desktop97.jpg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-location-form">
                <h1 className="card-title">New Hat Location Form</h1>
                <p className="mb-3">
                  Please describe your new location and what closet you would like to store your hats in.
                </p>

                {/* foreign key relationship code from ShoesForm
                <div className="mb-3">
                  <select onChange={handleBinChange} required id="bin" name="bin" className="form-select" >
                    <option value="">Choose a closet to store your shoes inside a bin.</option>
                    {bins.map((bin) => {
                    return (
                        <option key={bin.id} value={bin.href}> */}
                            {/* treat the value as it is in insomnia/js/create shoes
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                        {/* {bin.closet_name}
                        </option>
                        );
                    })}
                  </select>
                </div> */}



                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleClosetNameChange} value={closetName} required placeholder="closet name" type="text" id="closet_name" name="closet_name" className="form-control" />
                      <label htmlFor="closet_name">Closet name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleSectionNumberChange} value={sectionNumber} required placeholder="section number" type="section_number" id="section_number" name="section_number" className="form-control" />
                      <label htmlFor="section_number">Section number</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleShelfNumberChange} value={shelfNumber} required placeholder="shelf number" type="text" id="shelf_number" name="shelf_number" className="form-control" />
                      <label htmlFor="shelf_number">Shelf number</label>
                    </div>
                  </div>


                </div>
                <button className="btn btn-lg btn-primary">Create hat location!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! Your location has been saved.
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default LocationsForm;
