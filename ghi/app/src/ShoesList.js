import React from 'react';
import { Link } from "react-router-dom";



function ShoesList({ shoes, getShoes }) {
    const deleteShoe = async (id) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
            method: "delete",
        })
        if (response.ok) {
            getShoes()
        }
    }

    if (shoes === undefined) {
        return null
    }




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Shoe brand name</th>
              <th>Shoe model name</th>
              <th>Shoe color</th>
              <th>Bin</th>
              <th>Photo</th>
              <th>Delete pair</th>


            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.bin }</td>
                        <td><img src={ shoe.picture_url } className="img-thumbnail shoes" width="300"></img></td>
                        <td>
                            <button type="button" value={shoe.id} onClick={() => deleteShoe(shoe.id)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default ShoesList;
