import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';

import BinsList from './BinsList';
import BinForm from './BinForm';
import LocationForm from './LocationForm';

import HatsList from './HatsList';
import HatForm from './HatForm';
import { useState, useEffect } from "react";




function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])
  const [bins, setBins] = useState([])
  const [locations, setLocations] = useState([])

  const getShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const shoes = data.shoes
        setShoes(shoes)
      }
  }

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const hats = data.hats
        setHats(hats)
      }
  }

  const getBins = async () => {
    const url = 'http://localhost:8100/api/bins/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const bins = data.bins
        setBins(bins)
      }
  }

  const getLocations = async () => {
    const url = 'http://localhost:8100/api/locations/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const locations = data.locations
        setLocations(locations)
      }
  }

  useEffect(() => {
    getShoes();
    getHats();
    getBins();
    getLocations();
  }, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="shoes">
              <Route index element={<ShoesList shoes={shoes} />} />
              <Route path='new' element={<ShoeForm />} />
            </Route>

            <Route path="hats">
              <Route index element={<HatsList hats={hats} />} />
              <Route path='new' element={<HatForm />} />
            </Route>

            <Route path="bins">
              <Route index element={<BinsList bins={bins} />} />
              <Route path='new' element={<BinForm />} />
            </Route>

            <Route path="locations/new" element={<LocationForm />} />


        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
