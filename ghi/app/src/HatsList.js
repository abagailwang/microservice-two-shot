import React from 'react';
import { Link } from "react-router-dom";



function HatsList({ hats, getHats }) {
    const deleteHat = async (id) => {
        const response = await fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: "delete",
        })
        if (response.ok) {
            getHats()
        }
    }

    if (hats === undefined) {
        return null
    }




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Hat Fabric</th>
              <th>Hat Style</th>
              <th>Hat Color</th>
              <th>Location</th>
              <th>Photo</th>
              <th>Delete hat</th>


            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.style }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location }</td>
                        <td><img src={ hat.picture_url } className="img-thumbnail hats" width="300"></img></td>
                        <td>
                            <button type="button" value={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default HatsList;
