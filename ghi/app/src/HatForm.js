import React, { useEffect, useState } from 'react';

function HatsForm(props) {
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        console.log(data);

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabric('');
            setStyle('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }

    }







    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://y.yarn.co/1169e237-fe03-4dd7-a091-abacd62bf8b9_text.gif" />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-hat-form">
                                <h1 className="card-title">New hat! New hat!</h1>
                                <p className="mb-3">
                                    Please describe your new hat and where you would like to store them.
                                </p>

                                <div className="mb-3">
                                    <select onChange={handleLocationChange} required id="location" name="location" className="form-select" >
                                        <option value="">Choose a closet to store your hat inside a location.</option>
                                        {locations.map((location) => {
                                            return (
                                                <option key={location.id} value={location.href}>
                                                    {/* treat the value as it is in insomnia/js/create shoes
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                                                    {location.closet_name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Now, tell us about your new hat.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleFabricChange} value={fabric} required placeholder="Hat fabric" type="text" id="fabric" name="fabric" className="form-control" />
                                            <label htmlFor="name">Hat fabric</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleStyleChange} value={style} required placeholder="Hat style" type="style" id="style" name="style" className="form-control" />
                                            <label htmlFor="style">Hat style</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleColorChange} value={color} required placeholder="Hat color" type="text" id="color" name="color" className="form-control" />
                                            <label htmlFor="color">Hat color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handlePictureUrlChange} value={pictureUrl} required placeholder="Picture url" type="picture_url" id="picture_url" name="picture_url" className="form-control" />
                                            <label htmlFor="picture_url">Hat photo url</label>
                                        </div>
                                    </div>

                                </div>
                                <button className="btn btn-lg btn-primary">Enter hat!</button>
                            </form>
                            <div className="alert alert-success d-none mb-0" id="success-message">
                                Congratulations! Your hat has been saved.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default HatsForm;
