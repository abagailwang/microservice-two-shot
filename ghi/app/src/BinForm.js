import React, {useEffect, useState} from 'react';

function BinsForm(props) {
    const [closetName, setClosetName] = useState('');
    const [binNumber, setBinNumber] = useState('');
    const [binSize, setBinSize] = useState('');


    const handleClosetNameChange = (event) => {
        const value = event.target.value;
        setClosetName(value);
    }

    const handleBinNumberChange = (event) => {
        const value = event.target.value;
        setBinNumber(value);
    }

    const handleBinSizeChange = (event) => {
        const value = event.target.value;
        setBinSize(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.closet_name = closetName;
        data.bin_number = binNumber;
        data.bin_size = binSize;

        console.log(data);

        const binUrl = 'http://localhost:8100/api/bins/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            setClosetName('');
            setBinNumber('');
            setBinSize('');
        }

    }






// foreign key relationship code from ShoesForm
    // const fetchData = async () => {
    //     const url = 'http://localhost:8100/api/bins/';
    //     const response = await fetch(url);
    //     if (response.ok) {
    //       const data = await response.json();
    //       setBins(data.bins);
    //     }
    //   }

    //   useEffect(() => {
    //     fetchData();
    //   }, []);


return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://media.tenor.com/WT-wzYe3nUcAAAAd/overboard-secret.gif" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-bin-form">
                <h1 className="card-title">New Shoe Bin Form</h1>
                <p className="mb-3">
                  Please describe your new bin and what closet you would like to store your shoes in.
                </p>

                {/* foreign key relationship code from ShoesForm
                <div className="mb-3">
                  <select onChange={handleBinChange} required id="bin" name="bin" className="form-select" >
                    <option value="">Choose a closet to store your shoes inside a bin.</option>
                    {bins.map((bin) => {
                    return (
                        <option key={bin.id} value={bin.href}> */}
                            {/* treat the value as it is in insomnia/js/create shoes
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                        {/* {bin.closet_name}
                        </option>
                        );
                    })}
                  </select>
                </div> */}



                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleClosetNameChange} value={closetName} required placeholder="closet name" type="text" id="closet_name" name="closet_name" className="form-control" />
                      <label htmlFor="closet_name">Closet name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleBinNumberChange} value={binNumber} required placeholder="bin number" type="bin_number" id="bin_number" name="bin_number" className="form-control" />
                      <label htmlFor="bin_number">Bin number</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleBinSizeChange} value={binSize} required placeholder="bin size" type="text" id="bin_size" name="bin_size" className="form-control" />
                      <label htmlFor="bin_size">Bin size</label>
                    </div>
                  </div>


                </div>
                <button className="btn btn-lg btn-primary">Create bin!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! Your bin has been saved.
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default BinsForm;
