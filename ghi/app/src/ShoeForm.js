import React, {useEffect, useState} from 'react';

function ShoesForm(props) {
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        //create an empty JSON object
        const data = {};


        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }

    }







    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);


return (
  <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="https://media.tenor.com/Pd0li3N3iHAAAAAC/new-new-shoes.gif" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <h1 className="card-title">New shoes! New shoes!</h1>
                <p className="mb-3">
                  Please describe your new shoes and where you would like to store them.
                </p>

                <div className="mb-3">
                  <select onChange={handleBinChange} required id="bin" name="bin" className="form-select" >
                    <option value="">Choose a closet to store your shoes inside a bin.</option>
                    {bins.map((bin) => {
                    return (
                        <option key={bin.id} value={bin.href}>
                            {/* treat the value as it is in insomnia/js/create shoes
                            treat the key as something react can use, since there are multiple
                            conferences with the same exact name in my data base, we should refer to idx to make it unique  */}
                        {bin.closet_name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about your new shoes.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleManufacturerChange} value={manufacturer} required placeholder="Shoe brand" type="text" id="manufacturer" name="manufacturer" className="form-control" />
                      <label htmlFor="name">Shoe brand</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleModelNameChange} value={modelName} required placeholder="Shoe model" type="model_name" id="model_name" name="model_name" className="form-control" />
                      <label htmlFor="model_name">Shoe model</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleColorChange} value={color} required placeholder="Shoe color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Shoe color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handlePictureUrlChange} value={pictureUrl} required placeholder="Picture url" type="picture_url" id="picture_url" name="picture_url" className="form-control" />
                      <label htmlFor="picture_url">Shoe photo url</label>
                    </div>
                  </div>

                </div>
                <button className="btn btn-lg btn-primary">Enter shoe!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! Your pair of shoes has been saved.
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
);

}

export default ShoesForm;
